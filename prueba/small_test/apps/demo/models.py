from django.db import models


class Empresa(models.Model):
    nom = models.CharField(verbose_name="Nombre", max_length=30)
    dire = models.CharField(verbose_name="Direccion", max_length=50)
    nit = models.CharField(verbose_name="Nit", max_length=50)
    fcs = models.DateTimeField()
    tel = models.IntegerField(verbose_name="Tel")
    email = models.EmailField()

    def __str__(self):
        return "{}|{}|{}".format(self.nit, self.fcs, self.nom)